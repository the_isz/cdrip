#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import queue
import threading
import stat
import subprocess
import os
import os.path
import glob
import json
import collections
import codecs
import tempfile
import pydoc
import argparse
import functools
import time

import discid
import musicbrainzngs

import eyed3
import eyed3.id3

CDINFO_FILENAME = "cdinfo.json"

# Notes on ID3 libraries other than eyed3:
#
# * pyid3lib sets ID3v1 tags automatically and might not be maintained anymore.
#   Also, it seems to have problem with utf-8 strings which are the default for
#   mp3 files from amazon.
# * mutagen didn't produce ID3 headers recognizable to id3v2. This is probably
#   due to it using ID3 version 2.4, but I couldn't find an option to set a
#   different version.
#   Also, it's more complicated to use than eyed3 or pyid3lib.


class NoEntryFoundException(Exception):
  pass


class NotImplementedError(Exception):
  pass


def convert_release_to_result(release, medium):
  result = collections.OrderedDict \
      ( ( ( "title"      , release["title"]                )
        , ( "artist"     , release["artist-credit-phrase"] )
        , ( "date"       , release["date"]                 )
        , ( "genre"      , 12                              )
        , ( "disc_index" , int(medium["position"])         )
        , ( "disc_total" , medium["disc-count"]            )
        , ( "tracks"     , []                              )
        )
      )

  for track in sorted \
    ( release["medium-list"][0]["track-list"]
    , key=lambda t: int(t["position"])
    ):
    result["tracks"].append \
      ( { "title" : track["recording"]["title"]
        , "artist": track["artist-credit-phrase"]
        }
      )

  return result


def read_release_from_device(device):
  """
  Return release information for audio disc in device.

  Read the disc ID of the disc in the given device, then retrieve matching
  release information from musicbrainz.org and return it as a dictionary in the
  following form:

  { "title"     : <unicode>
  , "artist"    : <unicode>
  , "date"      : <unicode>
  , "genre"     : <int>
  , "disc_index": <int>
  , "disc_total": <int>
  , "tracks"    :
    [ { "title": <unicode>
      , "artist": <unicode>
      }
    ]
  }

  @param device: Path to the (block) device.
  @return: A dictionary with the release information.
  @raise discid.DiscError: Reading the disc failed
         discid.NotImplementedError: Reading not supported on this platform
         NoEntryFoundException: No matching data found on musicbrainz.org
         NotImplementedError: Result from musicbrainz.org in unhandled format
  """
  disc = discid.read(device)

  musicbrainzngs.set_useragent("cdrip", "3.0", "https://github.com/the-isz/cdrip")
  releases = musicbrainzngs.get_releases_by_discid \
      ( disc.id
      , toc=disc.toc_string
      , includes=["artists", "recordings", "artist-credits"]
      )

  if "disc" in releases:
    # Exact match
    #
    # Each release can consist of multiple media (e.g. CD+DVD, CD+Vinyl or
    # even 4 CD+1 CD).
    #
    # Each medium can consist of multiple discs.
    #
    # We need to find our disc among these. What's confusing is that for each
    # medium, only one track list is included, even if it consists of multiple
    # discs.
    # Luckily, it seems if the disc we looked for is one of multiple discs,
    # only its tracks are included, not the others'.

    # TODO: We might want to have the user choose from the releases instead of
    #       picking the first match.

    for release in releases["disc"]["release-list"]:
      for medium in release["medium-list"]:
        for m_disc in medium["disc-list"]:
          if m_disc["id"] != disc.id:
            continue

          return convert_release_to_result(release, medium)

  elif "cdstub" in releases:
    # No exact match, but CD Stub match

    result = collections.OrderedDict \
        ( ( ( "title"      , releases["cdstub"]["title"]  )
          , ( "artist"     , releases["cdstub"]["artist"] )
          , ( "date"       , releases.get("date", "")     )
          , ( "genre"      , 12                           )
          , ( "disc_index" , 1                            )
          , ( "disc_total" , 1                            )
          , ( "tracks"     , []                           )
          )
        )

    for track in releases["cdstub"]["track-list"]:
      result["tracks"].append \
        ( { "title" : track["title"]
          , "artist": releases["cdstub"]["artist"]
          }
        )

    print("WARNING: Match is from a CD stub!")
    print("See https://musicbrainz.org/doc/CD%20Stub for details")
    print("Consider adding this as a proper release using this link:\n {}".format \
        ( disc.submission_url
        ))

    return result

  elif "release-list" in releases:
    # Fuzzy match against TOC
    if len(releases["release-list"]) > 0:
      # Same as exact match, except that we don't have the disc ID to compare,
      # so we need to just take the first one we find.

      # TODO: As with exact matches, have user pick one.

      return convert_release_to_result \
          ( releases["release-list"][0]
          , releases["release-list"][0]["medium-list"][0]
          )

  raise NoEntryFoundException(
      "Couldn't find DiscID {} in musicbrainz.org database.\n"
      "If you'd like to add it, use this link:\n  {}".format \
        ( disc.id
        , disc.submission_url
        ))


def playlist_filename_from_release(release):
  """
  Return the playlist filename for the release.

  @param release: A dictionary as returned by read_release_from_device()
  @return: The playlist filename
  """
  return "{}.m3u".format(release["title"] if len(release["title"]) != 0 else "playlist")


def track_filename_from_release(release, track_idx):
  """
  Return the track filename for the track_idx'th track of release.

  @param release: A dictionary as returned by read_release_from_device()
  @param track_idx: The index of the track
  @return: The track filename
  """
  # / is not allowed in a file name, escaping is futile :)
  # FIXME: handle track names starting with "-"
  return "{}.mp3".format(release["tracks"][track_idx]["title"].replace("/", "-"))


def print_release_info(release, outputFile=sys.stdout):
  """
  Prints information on the album.

  @param release: A dictionary as returned by read_release_from_device()
  @param outputFile: A file-like object to print to
  """
  print \
    ( """
######### detected values: ########################################
Album:  {}
Artist: {}
Date:   {}
Genre:  {} ({})
Disc:   {} / {}

Tracks:""".format \
      ( release["title"]
      , release["artist"]
      , release["date"]
      , release["genre"]
      , eyed3.id3.genres[release["genre"]]
      , release["disc_index"]
      , release["disc_total"]
      )
    , file=outputFile
    )

  print_track_artist = \
    any([t["artist"] != release["artist"] for t in release["tracks"]])

  for i, track in enumerate(release["tracks"], start=1):
    print \
      ( "{}. {}{}".format \
        ( i
        , "" if not print_track_artist else "{} - ".format(track["artist"])
        , track["title"]
        )
      , file=outputFile
      )

  print \
    ( """
Using playlist file: "{}"
###################################################################""".format \
      ( playlist_filename_from_release(release))
    , file=outputFile
    )


# which function is from:
# http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def which(program):
  def is_exe(fpath):
    return os.path.exists(fpath) and os.access(fpath, os.X_OK)

  fpath, fname = os.path.split(program)
  if fpath:
    if is_exe(program):
      return program
  else:
    for path in os.environ["PATH"].split(os.pathsep):
      exe_file = os.path.join(path, program)
      if is_exe(exe_file):
        return exe_file

  return None


def term_size():
  """
  Returns the terminal's width and height.

  @return: a tuple containing the terminal's width and height, defaulting to
           C{(80, 25)} if the size could not be determined.
  """
  default = (80, 25)

  if not which("stty"):
    return default

  tmp = tempfile.TemporaryFile()

  rcode = \
      subprocess.call(["stty", "size"], stdout=tmp, stderr=subprocess.STDOUT)

  if 0 != rcode:
    return default

  tmp.seek(0)
  result = tmp.readline()
  tmp.close()

  return tuple(int(x) for x in reversed(result.split()))


def print_genres():
  """
  Print a genre listing.

  Uses python's built-in paging mechanism to print a genre listing to the
  terminal.
  """
  genres = dict(filter \
    ( lambda x: type(x[1]) is str and x[1] != "<not-set>"
    , eyed3.id3.genres.items()
    ))

  longest = max([len(g) for g in genres.values()])

  # 3 for genre ID, 2*1 for spaces.
  genre_cols = term_size()[0] // (3 + 1 + 1 + longest)

  def pack_n(container, n):
    """
    Generator returning slices of n from container's elements.
    """
    idx = 0
    while idx < len(container)-n:
      yield container[idx:idx+n]
      idx += n
    yield container[idx:]

  pydoc.pager \
    ( "\n".join \
      (
        [ " ".join(x) for x in pack_n \
          ( [ "{:3} {:{width}}".format(a[0], a[1], width=longest) for a in genres.items()
            ]
          , genre_cols
          )
        ]
      )
    )


def get_genre():
  """
  Print available genres using print_genres and ask the user to choose one.

  @return: The genre ID selected by the user
  """
  while True:
    print_genres()
    result = input("Enter genre number, anything else to see list again: ")
    number = 0

    try:
      number = int(result)
    except ValueError:
      continue

    if number in eyed3.id3.genres:
      return number


def cd_paranoia_thread_func(track_queue, print_func, device, num_tracks, stop_event):
  """
  Run cdparanoia command-line tool on device, once for each track. Each .wav
  file fully read from the CD will be put on track_queue.

  @param track_queue: queue.Queue object on which to put the finished .wav files
  @param print_func: A function taking one string argument for outputting to
                     console
  @param device: The device path on which to call cdparanoia
  @param num_tracks: The number of tracks on the CD
  @param stop_event: An event telling the thread to stop immediately
  """
  # TODO:
  # Maybe changing this to only one call to cdparanoia and watching for when
  # another .wav file lands on the disc is better, performance-wise, as it
  # would reduce the overhead of initializing and shutting down cdparanoia
  # every time.

  with open(os.devnull, "w") as fnull:
    for i in range(1, num_tracks + 1):
      if stop_event.is_set():
        break

      result = subprocess.call \
        ( "cdparanoia -d {} -B -w".format(device).split(" ") + ["{0}-{0}".format(i)]
        , stdout=fnull
        , stderr=subprocess.STDOUT
        )

      if 0 != result:
        print_func("Error reading track {}: cdparanoia returned {}".format(i, result))
        continue

      track_queue.put((i, "track{:02}.cdda.wav".format(i)))


def lame_thread_func(track_queue, print_func, release, stop_event, thread_id):
  """
  Run lame command-line tool on each file name put onto track_queue until None
  is returned from track_queue.

  @param track_queue: queue.Queue object from which to get .wav files
  @param print_func: A function taking one string argument for outputting to
                     console
  @param release: A dictionary as returned by read_release_from_device()
  @param stop_event: An event telling the thread to stop immediately
  @param thread_id: An identifier for the thread (used in status messages)
  """
  print_func("Start lame thread {}...".format(thread_id))

  while True:
    if stop_event.is_set():
      break

    try:
      task = track_queue.get_nowait()

    except queue.Empty:
      time.sleep(0.25)
      continue

    if None is task:
      break

    track_num, file_name = task
    # The wav file's indices are 1-based.
    track_file = track_filename_from_release(release, track_num - 1)

    print_func("lame thread {}: Convert \"{}\"...".format(thread_id, track_file))

    with open(os.devnull, "w") as fnull:
      # Popen needs a list as the first parameter with each program argument
      # being a separate list entry...
      lameProcess = subprocess.Popen \
        ( "lame -h {}".format(file_name).split(" ") + [track_file]
        , stdout=fnull
        , stderr=subprocess.STDOUT
        )

      result = lameProcess.wait()

      track_queue.task_done()

      if 0 != result:
        print_func \
          ( "lame thread {}: Error converting \"{}\": lame returned {}".format \
            ( thread_id
            , track_file
            , result
            )
          )

  print_func("lame thread {}: Exit".format(thread_id))


def print_func(lock, message):
  with lock:
    print(message)


def createArgumentParser():
  """
  Return argparse.ArgumentParser instance for cdrip

  This is done in a separate function to also use it for manpage generation via
  an external tool.
  """
  parser = argparse.ArgumentParser(prog="cdrip")
  parser.add_argument \
    ( "-u", "--use-existing"
    , dest="use_exist"
    , action="store_true"
    , default=False
    , help="Use existing cdinfo.json file in current directory"
    )
  parser.add_argument \
    ( "device"
    , metavar="<device>"
    , help="Device with audio CD"
    )
  return parser


def main():
  args = createArgumentParser().parse_args()

  # Check for cdparanoia and lame

  for prog in ["cdparanoia", "lame"]:
    if not which(prog):
      print("Couldn't find {} in PATH.".format(prog))
      return 1

  # Perform device checks

  if not os.path.exists(args.device):
    print("\"{}\" does not exist.".format(args.device))
    return 1

  device_mode = os.stat(args.device)[stat.ST_MODE]

  if not stat.S_ISBLK(device_mode):
    print("\"{}\" is not a block-device.".format(args.device))
    return 1

  release = None

  # Read track info

  if args.use_exist:
    if not os.path.exists(os.path.join(".", CDINFO_FILENAME)):
      print("Couldn't find \"{}\".".format(CDINFO_FILENAME))
      return 1

    with open(CDINFO_FILENAME, "r") as f:
      release = json.load(f, object_pairs_hook=collections.OrderedDict)

  else:
    release = read_release_from_device(args.device)

    # Ask the user for the genre
    release["genre"] = get_genre()

  print_release_info(release)

  # Ask whether user wants to edit

  print(
      """
Please check if you are pleased with the detected values.

Would you like me to continue with these values or do you want to edit?
      """)

  while True:
    # For python 3.0 and above, change the following to input() only!
    result = input("Please enter: [c]ontinue / [e]dit / [q]uit: ").lower()
    if "c" == result:
      break

    elif "e" == result:
      print("Edit {} and run {} again with the -u option.".format \
        ( CDINFO_FILENAME
        , sys.argv[0]
        ))

      with open(CDINFO_FILENAME, "w") as f:
        json.dump \
          ( release
          , f
          , ensure_ascii=False
          , indent=4
          )

      return 0

    elif "q" == result:
      return 0

  # Start cdparanoia thread

  print("\nStart processing...")

  track_queue = queue.Queue()

  print_lock = threading.Lock()
  stop_event = threading.Event()

  cdp_thread = threading.Thread \
    ( target=cd_paranoia_thread_func
    , kwargs=\
      { "track_queue": track_queue
      , "device"     : args.device
      , "print_func" : functools.partial(print_func, print_lock)
      , "num_tracks" : len(release["tracks"])
      , "stop_event" : stop_event
      }
    )
  cdp_thread.start()

  # Start lame threads

  # I doubt more than 2 will ever be needed.
  num_lame_threads = 2
  lame_threads = []

  for i in range(num_lame_threads):
    lame_thread = threading.Thread \
      ( target=lame_thread_func
      , kwargs=\
        { "track_queue": track_queue
        , "release"    : release
        , "print_func" : functools.partial(print_func, print_lock)
        , "stop_event" : stop_event
        , "thread_id"  : i + 1
        }
      )
    lame_threads.append(lame_thread)
    lame_thread.start()

  # Wait for threads to finish

  try:
    while True:
      if not cdp_thread.is_alive():
        break
      # Use sleep instead of join to allow for signal handling
      time.sleep(0.25)

    for i in range(num_lame_threads):
      # Shut down the lame threads once they're done... "injecting poison"
      track_queue.put(None)

    for lame_thread in lame_threads:
      while True:
        if not lame_thread.is_alive():
          break
        # Use sleep instead of join to allow for signal handling
        time.sleep(0.25)

  except KeyboardInterrupt:
    stop_event.set()
    print("Keyboard interrupt caught. Stopping as soon as possible...")
    cdp_thread.join()
    for lame_thread in lame_threads:
      lame_thread.join()
    return 1

  # Set id3v2 tags

  print("\nSet ID3 tags and create .m3u playlist...")

  with codecs.open(playlist_filename_from_release(release), "w", "utf-8") as \
    playlist_file:

    for track_idx in range(len(release["tracks"])):

      track_file = track_filename_from_release(release, track_idx)

      if not os.path.isfile(track_file):
        print("Skip missing file \"{}\"...".format(track_file))

      track = release["tracks"][track_idx]

      tag = eyed3.id3.Tag()
      tag.artist = track["artist"]
      tag.album = release["title"]
      tag.genre = release["genre"]
      tag.recording_date = release["date"]
      tag.track_num = (track_idx + 1, len(release["tracks"]))
      tag.title = track["title"]
      tag.disc_num = (release["disc_index"], release["disc_index"])
      # Using version 2.4 here seems to prevent mpd from properly retrieving
      # the genre... or eyed3 from properly writing it.
      # Using version 2.3 on the other hand seems not to be in conflict with
      # the header using version 2.4, so we'll stick with that:
      tag.save \
        ( filename=track_file
        , version=eyed3.id3.ID3_V2_3
        , encoding="utf-8"
        )

      playlist_file.writelines([track_file, "\n"])

  # clean up

  if args.use_exist:
    try:
      os.remove(CDINFO_FILENAME)

    except OSError:
      print("Failed to delete \"{}\"".format(CDINFO_FILENAME))

  for filename in glob.glob("track??.cdda.wav"):
    try:
      os.remove(filename)

    except OSError:
      print("Failed to delete \"{}\"".format(filename))

if __name__ == '__main__':
  sys.exit(main())

# vim: set sw=2 et:
