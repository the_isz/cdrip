# cdrip(1) -- A cdparanoia, lame and musicbrainz.org frontend

## SYNOPSIS

`cdrip [-h] [-u] <device>`

## DESCRIPTION

`cdrip` rips audio CDs using cdparanoia(1), then converts them to mp3 using
lame(1). The files are created in the current directory.

Track information is downloaded from musicbrainz.org and used to name the
generated .mp3 files and set id3 version 2.3 tags on them.
Additionally, an .m3u playlist is generated.

After all work is done, the directory is cleared of intermediate files.

## OPTIONS

* `<device>`:
  Device with audio CD

* `-h`, `--help`:
  Show this help message and exit

* `-u`, `--use-existing`
  Use existing `cdinfo.json` file in current directory

## AUTHOR

`cdrip` was written by Timo Merkel <the_isz@gmx.de>.
